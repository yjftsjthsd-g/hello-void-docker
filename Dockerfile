FROM voidlinux/voidlinux
RUN xbps-install -Suy base-devel wget
WORKDIR /root
RUN wget https://mirrors.sarata.com/gnu/hello/hello-2.12.tar.gz && tar xf hello-2.12.tar.gz
WORKDIR /root/hello-2.12
RUN ./configure && make
